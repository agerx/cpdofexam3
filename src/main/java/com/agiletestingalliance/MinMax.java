package com.agiletestingalliance;

public class MinMax {

    public int getMin(int first, int second) {
        if (first > second) {
            return second;
	}
        else {
            return first;
	}
    }

}
