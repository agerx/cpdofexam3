//package com.amdocs.agiletestingalliance;
package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
 
public class CPDOFETest {


    @Test
    public void testDuration() throws Exception {
	String expectedResult = "CP-DOF is designed specifically for corporates and working professionals alike. If you are a corporate and can not dedicate full day for training, then you can opt for either half days course or  full days programs which is followed by theory and practical exams.";
	Duration duration = new Duration ();
	String actualResult = duration.dur();
        assertEquals(expectedResult, actualResult);
        
    }

    @Test
    public void testUsefullness() throws Exception {
	String expectedResult = "DevOps is about transformation, about building quality in, improving productivity and about automation in Dev, Testing and Operations. <br> <br> CP-DOF is a one of its kind initiative to marry 2 distinct worlds of Agile and Operations together. <br> <br> <b>CP-DOF </b> helps you learn DevOps fundamentals along with Continuous Integration and Continuous Delivery and deep dive into DevOps concepts and mindset.";
	Usefulness usefulness = new Usefulness ();
	String actualResult = usefulness.desc();
        assertEquals(expectedResult, actualResult);
        
    }

    @Test
    public void testAbout() throws Exception {
	String expectedResult = "CP-DOF certification program covers end to end DevOps Life Cycle practically. CP-DOF is the only globally recognized certification program which has the following key advantages: <br> 1. Completely hands on. <br> 2. 100% Lab/Tools Driven <br> 3. Covers all the tools in entire lifecycle <br> 4. You will not only learn but experience the entire DevOps lifecycle. <br> 5. Practical Assessment to help you solidify your learnings.";
	AboutCPDOF about = new AboutCPDOF ();
	String actualResult = about.desc();
        assertEquals(expectedResult, actualResult);
        
    }

    @Test
    public void testMin() throws Exception {
	int expectedResult = 0;
	MinMax minmax = new MinMax ();
	int actualResult = minmax.getMin(0,1);
        assertEquals(expectedResult, actualResult);
        
    }

    @Test
    public void testMax() throws Exception {
	int expectedResult = 0;
	MinMax minmax = new MinMax ();
	int actualResult = minmax.getMin(1,0);
        assertEquals(expectedResult, actualResult);
        
    }
    }
